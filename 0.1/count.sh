#!/usr/bin/env bash
find ./* \( -ipath ./cc -o -iname count.file \) -prune -o -type f -print | wc -l | tee count.file
