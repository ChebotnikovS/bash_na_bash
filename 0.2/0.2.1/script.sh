#!/usr/bin/env bash
sum=0
count=0
for line in $(cat test | awk '{print $3}'); do
    sum=$(( $sum + $line));
    count=$(($count+1))
done
average=$(( $sum / $count))
echo "Среднее арифметическое: $average"
